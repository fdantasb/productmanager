package br.com.targetit.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.targetit.model.ErrorDetails;
import br.com.targetit.service.exception.ProductNotFundException;

@ControllerAdvice
public class ResourceExceptionHandler {
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorDetails> MethodArgumentNotValidException(MethodArgumentNotValidException ex, HttpServletRequest request) {
		ErrorDetails body = new ErrorDetails();
		
		body.setStatus(400l);
		body.setTitle("This request can not be performed.");
		body.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
	}
	@ExceptionHandler(ProductNotFundException.class)
	public ResponseEntity<ErrorDetails> ProductNotFundException(ProductNotFundException ex, HttpServletRequest request) {
		
		ErrorDetails body = new ErrorDetails();
		body.setTitle(ex.getMessage());
		body.setStatus(404l);
		body.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(body);
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<ErrorDetails> IllegalArgumentException(IllegalArgumentException ex, HttpServletRequest request) {
		
		ErrorDetails body = new ErrorDetails();
		body.setTitle(ex.getMessage());
		body.setStatus(400l);
		body.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
	}
	
}
