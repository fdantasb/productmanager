package br.com.targetit.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
public class Product implements Serializable{

	
	private static final long serialVersionUID = 7448597396067642064L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonInclude(Include.NON_NULL)
	private Long id;
	@NotEmpty(message = "The name can't be empty")
	private String name;
	@ElementCollection
	private List<Blob> images;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentId")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Product parent;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Product getParent() {
		return parent;
	}
	public void setParent(Product parent) {
		this.parent = parent;
	}
	public List<Blob> getImages() {
		return images;
	}
	public void setImages(List<Blob> images) {
		this.images = images;
	}
	

}
