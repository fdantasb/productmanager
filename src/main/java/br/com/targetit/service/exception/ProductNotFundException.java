package br.com.targetit.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ProductNotFundException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public ProductNotFundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProductNotFundException(String message) {
		super(message);
	}
	
	

}
