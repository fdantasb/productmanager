package br.com.targetit.service;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.targetit.model.Product;
import br.com.targetit.model.SimpleProduct;
import br.com.targetit.repository.ProductRepository;
import br.com.targetit.service.exception.ProductNotFundException;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository repository;

	public Product save(Product product) {
		product.setId(null);
		setParent(product);
		return repository.save(product);
	}


	public List<Product> listAll() {
		return repository.findAll();
	}

	public List<SimpleProduct> listAllSimple() {
		List<Product> findAll = listAll();
		List<SimpleProduct> result = getSimpleProductList(findAll);
		
		return result;
	}

	private List<SimpleProduct> getSimpleProductList(List<Product> findAll) {
		List<SimpleProduct> list = new ArrayList<SimpleProduct>();
		SimpleProduct simpleProduct = null;

		if (findAll != null) {
			for (Product product : findAll) {
				simpleProduct = new SimpleProduct();
				simpleProduct.setName(product.getName());
				simpleProduct.setId(product.getId());
				list.add(simpleProduct);
			}
		}
		
		return list;
	}

	public Product getone(Long id) {
		Product product = repository.findOne(id);
		
		if (product == null) {
			throw new ProductNotFundException("There is no Product with the privided id.");
		}
		
		return product;
	}

	public void udpate(Product product) {
		isThereThisProduct(product.getId());
		setParent(product);
		repository.save(product);
	}

	private void isThereThisProduct(Long id) {
		getone(id);
	}

	public List<Product> getChildren(Long parentId) {
		Product parent = getone(parentId);
		return repository.findProductByParent(parent);
	}

	private void setParent(Product product) {
		if (product.getParent() != null) {
			Long parentId = product.getParent().getId();
			isThereThisProduct(parentId);
			if (product.getId() == parentId) {
				throw new IllegalArgumentException("The parent can not be the product.");
			}			
			Product parent = getone(parentId);
			product.setParent(parent);
		}
	}


	public List<Blob> getImages(Long id) {
		Product product = getone(id);
		return product.getImages();
	}
}
