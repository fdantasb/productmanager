package br.com.targetit.controller;

import java.net.URI;
import java.sql.Blob;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.targetit.model.Product;
import br.com.targetit.model.SimpleProduct;
import br.com.targetit.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService service;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<Void> createProduct(@Valid @RequestBody Product product) {
		product = service.save(product);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/product/{id}").buildAndExpand(product.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> list() {
		List<Product> listAll = service.listAll();
		
		if (CollectionUtils.isEmpty(listAll)) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
		
		return ResponseEntity.ok(listAll);
	}
	
	@RequestMapping(value = "/simpleList", method = RequestMethod.GET)
	public ResponseEntity<List<SimpleProduct>> simplelist() {
		List<SimpleProduct> simpleList = service.listAllSimple();
		
		if (CollectionUtils.isEmpty(simpleList)) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
		
		return ResponseEntity.ok(simpleList);
	}
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public ResponseEntity<Product> getOne(@PathVariable("id") Long id) {
		Product product = service.getone(id);
		return ResponseEntity.ok().body(product);
	}
	@RequestMapping(value = "/product/update", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody Product product) {
		service.udpate(product);
		return ResponseEntity.status(HttpStatus.OK).build();
	}
	
	@RequestMapping(value = "/product/{id}/child", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> getChildres(@PathVariable("id") Long id){
		List<Product> result = service.getChildren(id);
		
		if (CollectionUtils.isEmpty(result)) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
		
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/product/{id}/images", method = RequestMethod.GET)
	public ResponseEntity<List<Blob>> getImages(@PathVariable("id") Long id){
		List<Blob> result = service.getImages(id);
		
		if (CollectionUtils.isEmpty(result)) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
		
		return ResponseEntity.ok(result);
	}
	
}	
