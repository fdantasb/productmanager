package br.com.targetit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.targetit.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	List<Product> findProductByParent(Product parent);

}
