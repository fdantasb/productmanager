package br.com.targetit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.targetit.controller.ProductController;
import br.com.targetit.model.Product;
import br.com.targetit.model.SimpleProduct;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductManagerApplicationTests {
	
	@Autowired
	private ProductController controller;
		
	@Test
	public void successSave() throws Exception{
		
		Product product = new Product();
		product.setId(null);
		product.setName("iPhone");
		product.setImages(null);
		product.setParent(null);
		
		ResponseEntity<Void> createProduct = controller.createProduct(product);
		
		assertEquals(HttpStatus.CREATED, createProduct.getStatusCode());
		
	}
	
	@Test
	public void productNoName(){
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		Validator validator = validatorFactory.getValidator();
		
		Product product = new Product();
		product.setId(null);
		product.setName("");
		product.setImages(null);
		product.setParent(null);
		
		Set<ConstraintViolation<Product>> validate = validator.validate(product);
		
		assertThat(!validate.isEmpty());
		
	}
	
	@Test
	public void listAll() {
		ResponseEntity<List<Product>> list = controller.list();
		assertThat(list != null);
	}
	
	@Test
	public void listAllSimple() {
		ResponseEntity<List<SimpleProduct>> list = controller.simplelist();
		assertThat(list != null);
	}
	
	@Test
	public void updateProductSuccess(){
		Product product = new Product();
		product.setId(1l);
		product.setName("Samsung");
		product.setImages(null);
		product.setParent(null);
		
		ResponseEntity<Void> createProduct = controller.update(product);
		
		assertEquals(HttpStatus.OK, createProduct.getStatusCode());
	}
	
}
