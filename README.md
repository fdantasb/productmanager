# README #

### What is this repository for? ###

This respository it's an application for a job position for Java/Hybris.

### How do I get set up? ###

First of all, you need to run the test for this application, you can run it with maven, using command mvn:test. The test will provide one product to database.

To start the application you need to run the following command in maven: mvn spring-boot:run.

The following methods are available:


- Simple list of all products in dabase
/list],methods=[GET]
- A simple list of all products in database
/simpleList],methods=[GET]
- method to create a product using the following json: 
{
  "name" : "",
  "parent": "",
  "images" : ""
}

In the case to add a parent product, should be a product that exists in database, and a product can't be his own parent.
/create],methods=[POST]

- Get a product using the id
/product/{id}],methods=[GET]
- Get a list of child products using the parent id
/product/{id}/child],methods=[GET]
- Get a list of imagens using the parent id
/product/{id}/images],methods=[GET]
- update the product using the sabe json as create, just add the field for id.
/product/update],methods=[PUT]

You can use Postman (https://www.getpostman.com/) for using test cases:
https://www.getpostman.com/collections/3b189df92ab561d55750


### Who do I talk to? ###

My name is Flávio, and you can reach me at +55 11 985188588